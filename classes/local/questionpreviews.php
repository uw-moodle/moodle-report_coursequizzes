<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace report_coursequizzes\local;

defined('MOODLE_INTERNAL') || die();

global $CFG;
require_once($CFG->dirroot .'/question/previewlib.php');

/**
 * activequiz Attempt wrapper class to encapsulate functions needed to individual
 * attempt records
 *
 * @package     mod_activequiz
 * @author      John Hoopes <hoopes@wisc.edu>
 * @copyright   2014 University of Wisconsin - Madison
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class questionpreviews {

    /** @var \question_usage_by_activity $quba the question usage by activity for this attempt */
    protected $quba;

    /** @var \context_module $context The context for this attempt */
    protected $context;

    /** @var \stdClass The "attempt" to preview the questions selected */
    protected $attempt;

    /** @var  \question_preview_options $qoptions */
    protected $qoptions;

    /** @var  array $questions an array of question objects from the db for the questionids passed */
    protected $questions;

    /** @var array $attemptlayout The layout of the attempt  */
    protected $attemptlayout;

    /** @var  int $qnum The current question number */
    protected $qnum;

    /**
     * Construct the class.  Sets up the QUBA for previewing the questions for printing
     *
     * @param array $questionids An array of questionids
     * @param \context_module $context
     */
    public function __construct($questionids, $context = null) {
        global $DB;

        $this->context = $context;
        $this->attempt = new \stdClass();

        // create a new quba since we're creating a new attempt
        $this->quba = \question_engine::make_questions_usage_by_activity('mod_activequiz',
            $context);
        $this->quba->set_preferred_behaviour('deferredfeedback');

        // loop through the questionids and add them to the quba
        $attemptlayout = array();
        $questions = array();
        foreach($questionids as $qid){
            $q = \question_bank::load_question($qid);
            $questions[ $qid ] = $q;
            $attemptlayout [ $qid ] = $this->quba->add_question($q);
        }
        $this->questions = $questions;
        $this->attemptlayout = $attemptlayout;

        // start the questions in the quba
        $this->quba->start_all_questions();
    }

    /**
     * Get the slots (or attempt layout)
     *
     * @return array
     */
    public function getSlots(){
        return $this->attemptlayout;
    }

    /**
     * Render the specific question
     *
     * @param int $slot
     * @return string HTML fragment of the question
     */
    public function render_question($slot){
        $qid = array_search($slot, $this->attemptlayout);
        $question = $this->questions[$qid];

        $displayoptions = new \question_preview_options($question);

        return $this->quba->render_question($slot, $displayoptions);

    }

    /**
     * returns an integer representing the question number
     *
     * @return int
     */
    public function get_question_number() {
        if (is_null($this->qnum)) {
            $this->qnum = 1;

            return (string)1;
        } else {
            return (string)$this->qnum;
        }
    }

    /**
     * Adds 1 to the current qnum, effectively going to the next question
     *
     */
    protected function add_question_number() {
        $this->qnum = $this->qnum + 1;
    }

}