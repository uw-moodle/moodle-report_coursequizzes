<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace report_coursequizzes\local\controllers;

defined('MOODLE_INTERNAL') || die();

global $CFG;
require_once($CFG->libdir . '/questionlib.php');
require_once($CFG->dirroot . '/question/editlib.php');

/**
 * Index controller class
 *
 * @package   report_coursequizzes\local\controllers
 * @author    John Hoopes <john.hoopes@wisc.edu>
 * @copyright 2015 University of Wisconsin - Madison
 */
class printquestions{


    /** @var  string $action The action to perform for this request */
    protected $action;

    /** @var  \moodle_url $pageurl The current page URL */
    protected $pageurl;

    /** @var  \context_course $context The course context */
    protected $context;

    /** @var \question_edit_contexts $contexts*/
    protected $contexts;

    /** @var  \stdClass $course The course DB object */
    protected $course;

    /** @var  \report_coursequizzes\output\printquestions_renderer $renderer The renderer for the report index */
    protected $renderer;

    /**
     * Sets up the page with the required variables
     *
     * @param string $baseurl
     *
     * @throws \moodle_exception Throws exception on missing courseid
     */
    public function setup_page($baseurl) {
        global $DB, $PAGE;

        $courseid = optional_param('course', '', PARAM_INT);
        $this->action = optional_param('action', '', PARAM_ALPHANUMEXT);

        if($courseid === ''){
            throw new \moodle_exception('Missing course', 'report_coursequizzes');
        }
        // Get the course and context to perform login checks.
        $this->course = $DB->get_record('course', array('id' => $courseid), '*', MUST_EXIST);
        $this->context = \context_course::instance($this->course->id);

        require_login($this->course, false);
        require_capability('report/coursequizzes:view', $this->context);

        $this->pageurl = new \moodle_url($baseurl);
        $this->pageurl->remove_all_params();
        $this->pageurl->param('course', $courseid);
        $this->pageurl->param('action', $this->action);

        $PAGE->set_pagelayout('base');
        $PAGE->set_title(strip_tags($this->course->shortname . ': ' . get_string("pluginname", "report_coursequizzes")));
        $PAGE->set_heading($this->course->fullname);
        $PAGE->set_url($this->pageurl);

        $this->renderer = $PAGE->get_renderer('report_coursequizzes', 'printquestions');
    }

    /**
     *
     * Handles the request
     *
     */
    public function handle_request() {



        switch($this->action){


            default:

                $questionids = explode(',', optional_param('questionids' , '', PARAM_RAW) );

                $questionpreview = new \report_coursequizzes\local\questionpreviews($questionids, $this->context);

                $this->renderer->header($this->course);
                $this->renderer->render_previews($questionpreview);
                $this->renderer->footer();

                break;
        }
    }



}