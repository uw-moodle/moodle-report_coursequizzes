<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace report_coursequizzes\local\controllers;

defined('MOODLE_INTERNAL') || die();

global $CFG;
require_once($CFG->libdir . '/questionlib.php');
require_once($CFG->dirroot . '/question/editlib.php');

/**
 * Index controller class
 *
 * @package   report_coursequizzes\local\controllers
 * @author    John Hoopes <john.hoopes@wisc.edu>
 * @copyright 2015 University of Wisconsin - Madison
 */
class index{


    /** @var  string $action The action to perform for this request */
    protected $action;

    /** @var  \moodle_url $pageurl The current page URL */
    protected $pageurl;

    /** @var  \context_course $context The course context */
    protected $context;

    /** @var \question_edit_contexts $contexts*/
    protected $contexts;

    /** @var  \stdClass $course The course DB object */
    protected $course;

    /** @var  \report_coursequizzes\output\index_renderer $renderer The renderer for the report index */
    protected $renderer;

    /**
     * Sets up the page with the required variables
     *
     * @param string $baseurl
     *
     * @throws \moodle_exception Throws exception on missing courseid
     */
    public function setup_page($baseurl) {
        global $DB, $PAGE;

        $courseid = optional_param('course', '', PARAM_INT);
        $this->action = optional_param('action', '', PARAM_ALPHANUMEXT);

        if($courseid === ''){
            throw new \moodle_exception('Missing course', 'report_coursequizzes');
        }
        // Get the course and context to perform login checks.
        $this->course = $DB->get_record('course', array('id' => $courseid), '*', MUST_EXIST);
        $this->context = \context_course::instance($this->course->id);

        require_login($this->course, false);
        require_capability('report/coursequizzes:view', $this->context);

        // get the question edit level contexts for category building
        $this->contexts =  new \question_edit_contexts($this->context);
        $this->contexts->require_one_edit_tab_cap('editq');

        $this->pageurl = new \moodle_url($baseurl);
        $this->pageurl->remove_all_params();
        $this->pageurl->param('course', $courseid);
        $this->pageurl->param('action', $this->action);

        $PAGE->set_pagelayout('admin');
        $PAGE->set_context($this->context);
        $PAGE->set_url($this->pageurl);
        $PAGE->set_title(strip_tags($this->course->shortname . ': ' . get_string("pluginname", "report_coursequizzes")));
        $PAGE->set_heading($this->course->fullname);

        $this->renderer = $PAGE->get_renderer('report_coursequizzes', 'index');
    }

    /**
     *
     * Handles the request
     *
     */
    public function handle_request() {



        switch($this->action){


            default:

                $selectacategory = optional_param('selectacategory', '', PARAM_RAW);

                if(!empty($selectacategory)){
                    $parts = explode(",", $selectacategory);

                    if(count($parts) === 2){
                        $categoryid = $parts[0];
                        $contextid = $parts[1];

                        $newcontext = \context::instance_by_id($contextid);
                        $this->contexts =  new \question_edit_contexts($this->context);
                        $this->contexts->require_one_edit_tab_cap('editq');

                    }else{
                        throw new \moodle_exception('invalid category parameter');
                    }
                }else{
                    $categoryid = 0;
                    $contextid = $this->context->id;
                }


                // get category tree
                $categorytree = $this->get_category_tree($this->contexts, $categoryid, $contextid);

                $message = optional_param('message', null, PARAM_ALPHA);

                if ($message != null) {
                    $this->renderer->setMessage('success', get_string($message, 'report_coursequizzes'));
                }
                $this->renderer->index_header($this->course);
                $this->renderer->render_index_home($this->course,$categoryid, $contextid, $this->pageurl, $this->contexts, $categorytree);
                $this->renderer->index_footer();

                break;
        }
    }

    /**
     * @param \question_edit_contexts $contexts
     * @param int $selectedcategory
     * @param int $contextid
     * @throws \coding_exception
     */
    public function get_category_tree($contexts, $selectedcategory, $contextid){

        if($selectedcategory === 0){ // only get parents if no category/context is selected for
            $pcontexts = array();
            foreach ($contexts->having_one_edit_tab_cap('editq') as $context) {
                $pcontexts[] = $context->id;
            }
            $contextslist = join($pcontexts, ', ');
        }else{
            $contextslist = $contextid;
        }

        $categories = get_categories_for_contexts($contextslist);

        // Add a children element to categories
        foreach(array_keys($categories) as $id){
            $categories[$id]->children = array();
        }

        // loop through all of the categories and add children elements to their respective parents
        $toplevelcategoryids = array();
        foreach (array_keys($categories) as $id) {

            if (!empty($categories[$id]->parent) &&
                array_key_exists($categories[$id]->parent, $categories)) {
                $categories[$categories[$id]->parent]->children[] = $categories[$id];
            } else {
                $toplevelcategoryids[] = $id;
            }
        }

        $newcategories = array();
        // now go through them and unflatten the structure, this needs to be its own loop as category parents
        // might have other parents as the loop continues.  and so it is better to not unset in the previous loop
        foreach(array_keys($categories) as $id){

            if($selectedcategory && $id != $selectedcategory){
                unset($categories[$id]);
            }else if(!in_array($id, $toplevelcategoryids)){
                unset($categories[$id]);
            }
        }

        return $categories;

    }


}