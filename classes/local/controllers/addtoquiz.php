<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace report_coursequizzes\local\controllers;

defined('MOODLE_INTERNAL') || die();

global $CFG;
require_once($CFG->libdir . '/questionlib.php');
require_once($CFG->dirroot . '/question/editlib.php');
require_once($CFG->dirroot . '/mod/quiz/locallib.php');


/**
 * Add to Quiz controller class
 *
 * @package   report_coursequizzes\local\controllers
 * @author    John Hoopes <john.hoopes@wisc.edu>
 * @copyright 2015 University of Wisconsin - Madison
 */
class addtoquiz{


    /** @var  string $action The action to perform for this request */
    protected $action;

    /** @var  \moodle_url $pageurl The current page URL */
    protected $pageurl;

    /** @var  \context_course $context The course context */
    protected $context;

    /** @var \question_edit_contexts $contexts*/
    protected $contexts;

    /** @var  \stdClass $course The course DB object */
    protected $course;

    /** @var  \report_coursequizzes\output\addtoquiz_renderer $renderer The renderer for the report index */
    protected $renderer;

    /**
     * Sets up the page with the required variables
     *
     * @param string $baseurl
     *
     * @throws \moodle_exception Throws exception on missing courseid
     */
    public function setup_page($baseurl) {
        global $DB, $PAGE;

        $courseid = optional_param('course', '', PARAM_INT);
        $this->action = optional_param('action', '', PARAM_ALPHANUMEXT);

        if($courseid === ''){
            throw new \moodle_exception('Missing course', 'report_coursequizzes');
        }
        // Get the course and context to perform login checks.
        $this->course = $DB->get_record('course', array('id' => $courseid), '*', MUST_EXIST);
        $this->context = \context_course::instance($this->course->id);

        require_login($this->course, false);
        require_capability('report/coursequizzes:view', $this->context);

        // get the question edit level contexts for category building
        $this->contexts =  new \question_edit_contexts($this->context);
        $this->contexts->require_one_edit_tab_cap('editq');

        $this->pageurl = new \moodle_url($baseurl);
        $this->pageurl->remove_all_params();
        $this->pageurl->param('course', $courseid);
        //$this->pageurl->param('action', $this->action);

        $PAGE->set_pagelayout('report');
        $PAGE->set_title(strip_tags($this->course->shortname . ': ' . get_string("pluginname", "report_coursequizzes")));
        $PAGE->set_heading($this->course->fullname);
        $PAGE->set_url($this->pageurl);

        $this->renderer = $PAGE->get_renderer('report_coursequizzes', 'addtoquiz');

    }

    /**
     *
     * Handles the request
     *
     */
    public function handle_request()
    {
        global $DB, $PAGE;


        switch ($this->action) {

            case 'AddtoQuiz':
                $questionids = optional_param('questionids', '', PARAM_RAW);
                $questions = array();
                if (!empty($questionids))
                    $questions = $DB->get_records_select('question', 'id IN (' . $questionids . ')');

                $quizmods = get_course_mods($this->course->id);

                $quizmods = array_filter($quizmods, function ($q) {
                    return $q->modname == 'quiz';
                });

                $quizzes = [];
                foreach ($quizmods as $quizmod) {
                    $quizzes[$quizmod->id] = $DB->get_record('quiz', array('id' => $quizmod->instance));
                }
                $this->renderer->index_header($this->course);
                $this->renderer->render_add_to_quiz_home($this->course, $this->pageurl, $quizzes, $questions, $questionids);
//                $this->renderer->render_index_home($this->course,$categoryid, $contextid, $this->pageurl, $this->contexts, $categorytree);
                $this->renderer->index_footer();

                break;

            case 'Add':

                $questionids = optional_param('questionids', '', PARAM_RAW);
                $cmid = optional_param('cmid', '', PARAM_RAW);
                $questions = array();

                $questionids = explode(',', $questionids);
                list($sql, $params) = $DB->get_in_or_equal($questionids);

                if (!empty($questionids)) {
                    $questions = $DB->get_records_select('question', 'id ' . $sql, $params);
                } else {
                    throw new \moodle_exception('No questions found');
                }
                $quizid = optional_param('quiz', '', PARAM_RAW);
                if (!empty($quizid))
                    $quiz = $DB->get_record('quiz', array('id' => $quizid));

                $thiscontext = \context_module::instance($cmid);

                $category = question_make_default_categories(array('id' => $thiscontext));

                get_question_options($questions, true);
                //$addpermission = has_capability('moodle/question:add', $category->context);


                foreach ($questions as $question) {
                    $question->formoptions = new \stdClass();
                    $question->formoptions->canedit = question_has_capability_on($question, 'edit');
                    $question->formoptions->canmove = question_has_capability_on($question, 'move');
                    $question->formoptions->cansaveasnew =  (question_has_capability_on($question, 'view') || $question->formoptions->canedit);
                    $question->formoptions->repeatelements = $question->formoptions->canedit || $question->formoptions->cansaveasnew;
                    $formeditable = $question->formoptions->canedit || $question->formoptions->cansaveasnew || $question->formoptions->canmove;
                    if (!$formeditable) {
                        question_require_capability_on($question, 'view');
                    }

                    // If we are duplicating a question, add some indication to the question name.
                    $question->name = get_string('questionnamecopy', 'question', $question->name);
                    $question->beingcopied = true;

                    $question->formoptions->mustbeusable = true;

                    $qtypeobj = \question_bank::get_qtype($question->qtype);

                    $PAGE->set_pagetype('question-type-' . $question->qtype);

                    $contexts = new \question_edit_contexts($thiscontext);


                    $mform = $qtypeobj->create_editing_form('question.php', $question, $category, $contexts, $formeditable);

                    $toform = fullclone($question); // send the question object and a few more parameters to the form
                    $toform->category = "{$category->id},{$category->contextid}";
                    $toform->scrollpos = 0;
                    $toform->categorymoveto = $toform->category;


                    $toform->appendqnumstring = true;
                    $toform->returnurl = null;
                    $toform->makecopy = true;
                    $toform->cmid = $quiz->id;
                    $toform->courseid = $quiz->course;


                    $toform->inpopup = false;

                    $mform->set_data($toform);

                    if ($fromform = $toform) {
                        // If we are saving as a copy, break the connection to the old question.
                        $question->id = 0;
                        $question->hidden = 0; // Copies should not be hidden.


                        $fromform->category = $fromform->categorymoveto;


                        /// If we are moving a question, check we have permission to move it from
                        /// whence it came. (Where we are moving to is validated by the form.)
                        list($newcatid, $newcontextid) = explode(',', $fromform->category);
                        if (!empty($question->id) && $newcatid != $question->category) {
                            $contextid = $newcontextid;
                            question_require_capability_on($question, 'move');
                        } else {
                            $contextid = $category->contextid;
                        }

                        // Ensure we redirect back to the category the question is being saved into.

                        // We are acutally saving the question.
                        if (!empty($question->id)) {
                            question_require_capability_on($question, 'edit');
                        } else {
                            require_capability('moodle/question:add', \context::instance_by_id($contextid));
                            if (!empty($fromform->makecopy) && !$question->formoptions->cansaveasnew) {
                                print_error('nopermissions', '', '', 'edit');
                            }
                        }

                        $question = $qtypeobj->save_question($question, $fromform);

                        \question_bank::notify_question_edited($question->id);

                        \quiz_add_quiz_question($question->id, $quiz);
                        \quiz_delete_previews($quiz);
                        \quiz_update_sumgrades($quiz);

                    }

                }

                $returnurl = new \moodle_url('/report/coursequizzes/index.php');
                $returnurl->param('course', $this->course->id);
                $returnurl->param('message', 'questionsaddedsuccess');
                redirect($returnurl);


                break;

            default:


                break;
        }

    }


}