<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace report_coursequizzes\local\tableviews;

defined('MOODLE_INTERNAL') || die();
global $CFG;
require_once($CFG->libdir . '/tablelib.php');

/**
 * Table lib subclass for showing the course quizzes for printing
 *
 * @package     report_coursequizzes
 * @author      John Hoopes <hoopes@wisc.edu>
 * @copyright   2015 University of Wisconsin - Madison
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class coursequizzes extends \flexible_table implements \renderable {


    /** @var \stdClass $course The course we're getting the quizzes from */
    protected $course;


    /**
     * Contstruct this table class
     *
     * @param string                             $uniqueid The unique id for the table
     * @param \stdClass                          $course
     * @param \moodle_url                        $pageurl
     */
    public function __construct($uniqueid, $course, $pageurl) {

        $this->course = $course;
        $this->baseurl = $pageurl;

        parent::__construct($uniqueid);
    }


    /**
     * Setup the table, i.e. table headers
     *
     */
    public function setup() {
        // Set var for is downloading
        $isdownloading = $this->is_downloading();

        $columns = array(
            'quizname'     => get_string('quizname', 'report_coursequizzes'),
        );

        if (!$isdownloading) {
            $columns['printquiz'] = get_string('printquiz', 'report_coursequizzes');
        }

        $this->define_columns(array_keys($columns));
        $this->define_headers(array_values($columns));

        $this->sortable(true, 'quizname');
        $this->collapsible(true);

        $this->set_attribute('cellspacing', '0');
        $this->set_attribute('cellpadding', '2');
        $this->set_attribute('id', 'attempts');
        $this->set_attribute('class', 'generaltable generalbox');
        $this->set_attribute('align', 'center');

        parent::setup();
    }


    /**
     * Sets the data to the table
     *
     */
    public function set_data() {
        global $CFG, $OUTPUT;

        $download = $this->is_downloading();
        $tabledata = $this->get_data();

        foreach ($tabledata as $item) {

            $row = array();

            $row[] = $item->quizname;
            $row[] = '';

            $this->add_data($row);
        }

    }


    /**
     * Gets the data for the table
     *
     * @return array $data The array of data to show
     */
    protected function get_data() {
        global $DB;

        $data = array();
        $coursemods = get_course_mods($this->course->id);

        foreach ($coursemods as $coursemod) {

            if($coursemod->modname == "quiz"){
                $ditem = new \stdClass();

                $quiz = $DB->get_record('quiz', array('id' => $coursemod->instance));

                $ditem->quizname = $quiz->name;
                $data[] = $ditem;
            }

        }

        return $data;
    }

}
