<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace report_coursequizzes\output;

defined('MOODLE_INTERNAL') || die();

/**
 * Index renderer class
 *
 * @package   report_coursequizzes\output
 * @author    John Hoopes <john.hoopes@wisc.edu>
 * @copyright 2015 University of Wisconsin - Madison
 */
class addtoquiz_renderer extends \plugin_renderer_base{


    /**
     * show the header of the page, with tabs and message
     *
     * @param $course
     * @throws \coding_exception
     */
    public function index_header($course){

        $this->page->requires->jquery();
        // require HTML5 classList shim to add classlist functionality to older browsers
        $this->page->requires->js('/report/coursequizzes/js/classList.js');
        $this->page->requires->js('/report/coursequizzes/js/index.js');

        echo $this->output->header();
        //echo report_coursequizzes_view_tabs($course->id, 'coursequizzes');
        $this->showMessage();
    }

    /**
     * Output the footer of the page
     */
    public function index_footer(){
        echo $this->output->footer();
    }

    /**
     * Sets a page message to display when the page is loaded into view
     *
     * base_header() must be called for the message to appear
     *
     * @param string $type
     * @param string $message
     */
    public function setMessage($type, $message) {
        $this->pageMessage = array($type, $message);
    }

    /**
     * Shows the message to the page.  is called by index_header
     */
    protected function showMessage(){

        if(empty($this->pageMessage)){
            return;
        }

        if (!is_array($this->pageMessage)) {
            return; // return if it's not an array
        }
        switch ($this->pageMessage[0]) {
            case 'error':
                echo $this->output->notification($this->pageMessage[1], 'notifiyproblem');
                break;
            case 'success':
                echo $this->output->notification($this->pageMessage[1], 'notifysuccess');
                break;
            case 'info':
                echo $this->output->notification($this->pageMessage[1], 'notifyinfo');
                break;
            default:
                // unrecognized notification type
                break;
        }
    }

    /**
     * render the index home page
     *
     * @param \stdClass $course
     * @param \moodle_url $pageurl
     * @param $quizzes
     * @param $questions
     * @throws \coding_exception
     */
    public function render_add_to_quiz_home($course, $pageurl, $quizzes, $questions, $questionids) {

        echo \html_writer::start_div('questionrepository');

        echo $this->render_question_table($questions);

        echo $this->render_quiz_table($quizzes, $questionids, $pageurl);


        echo \html_writer::end_div();
    }



    protected function render_question_table($questions) {
        $questioncontent = '';
        $questioncontent .= \html_writer::start_div('selectedquestions');

        if (empty($questions)){
            $questioncontent .= 'No Questions Selected';
        } else {
            $questioncontent .= \html_writer::tag('h3', get_string('questionaddheading', 'report_coursequizzes'));
            foreach($questions as $question){
                $questionstring = $question->id . ' ' . $question->name ;
                $questioncontent .= \html_writer::span($questionstring, 'questionlistitem');
            }
        }
        $questioncontent .= \html_writer::end_div() . "\n";
        return $questioncontent;
    }


    protected function render_quiz_table ($quizzes, $questionids, $pageurl){

        $hiddenparams = '';
        foreach($pageurl->params() as $name => $param){
            $hiddenparams .= \html_writer::empty_tag('input', array('type' => 'hidden', 'name' => $name, 'value' => $param));
        }
        $hiddenparams .= \html_writer::empty_tag('input', array('type' => 'hidden', 'name' => 'questionids', 'value' => $questionids));

        $addtoquizurl = new \moodle_url('/report/coursequizzes/addtoquiz.php');

        $addtoquizsubmitbtn = \html_writer::empty_tag('input', array(
            'type' => 'submit',
            'name' => 'action',
            'value' => 'Add',
            'class' => 'form-submit',
            'formaction' => $addtoquizurl->out_omit_querystring()
        ));

        $buttonsdiv = $addtoquizsubmitbtn;

        $quizcontent = '';

        $quizcontent .= \html_writer::start_div('quiztable');

        if (empty($quizzes)){
            $quizcontent .= 'No Questions Selected';
        } else {
            $quizcontent .= \html_writer::tag('h3', get_string('quizaddheading', 'report_coursequizzes'));
            foreach($quizzes as $id => $quiz){
                $quizstring = " " . $quiz->name ;
                $quizcontent .= \html_writer::start_tag('form', array('method'=>'post', 'action'=>'Add'));
                $quizcontent .= \html_writer::span($buttonsdiv . $quizstring, 'questionlistitem');
                $quizcontent .= \html_writer::empty_tag('input', array('type' => 'hidden', 'name'=>'quiz', 'value'=>$quiz->id));
                $quizcontent .= \html_writer::empty_tag('input', array('type' => 'hidden', 'name'=>'cmid', 'value'=>$id));
                $quizcontent .= $hiddenparams;

                $quizcontent .= \html_writer::end_tag('form');

            }
        }

        $quizcontent .= \html_writer::end_div() . "\n";

        return $quizcontent;
    }



}