<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace report_coursequizzes\output;


defined('MOODLE_INTERNAL') || die();

/**
 * printquestions renderer class
 *
 * @package   report_coursequizzes\output
 * @author    John Hoopes <john.hoopes@wisc.edu>
 * @copyright 2015 University of Wisconsin - Madison
 */
class printquestions_renderer extends \plugin_renderer_base{


    /**
     * show the header of the page, with tabs and message
     *
     * @param $course
     * @throws \coding_exception
     */
    public function header($course){

        $this->page->requires->jquery();
        // require HTML5 classList shim to add classlist functionality to older browsers
        $this->page->requires->js('/report/coursequizzes/js/classList.js');

        $this->page->requires->css('/report/coursequizzes/css/printquestions.css');

        echo $this->output->header();
        //echo report_coursequizzes_view_tabs($course->id, 'coursequizzes');
        $this->showMessage();
    }

    /**
     * Output the footer of the page
     */
    public function footer(){
        echo $this->output->footer();
    }

    /**
     * Sets a page message to display when the page is loaded into view
     *
     * base_header() must be called for the message to appear
     *
     * @param string $type
     * @param string $message
     */
    public function setMessage($type, $message) {
        $this->pageMessage = array($type, $message);
    }

    /**
     * Shows the message to the page.  is called by index_header
     */
    protected function showMessage(){

        if(empty($this->pageMessage)){
            return;
        }

        if (!is_array($this->pageMessage)) {
            return; // return if it's not an array
        }
        switch ($this->pageMessage[0]) {
            case 'error':
                echo $this->output->notification($this->pageMessage[1], 'notifiyproblem');
                break;
            case 'success':
                echo $this->output->notification($this->pageMessage[1], 'notifysuccess');
                break;
            case 'info':
                echo $this->output->notification($this->pageMessage[1], 'notifyinfo');
                break;
            default:
                // unrecognized notification type
                break;
        }
    }

    /**
     * render the print questions page
     *
     */
    public function render_home($questionids){

        echo \html_writer::div(implode(',', $questionids));

    }

    /**
     * @param \report_coursequizzes\local\questionpreviews $questionpreviews
     */
    public function render_previews($questionpreviews){



        $output = '';

        $output .= \html_writer::start_div('report_coursequizzes_previewquestions');

        foreach($questionpreviews->getSlots() as $slot){

            $output .= \html_writer::start_div('questiondisplay');
            $output .= $questionpreviews->render_question($slot);
            $output .= \html_writer::end_div();

        }

        $output .= \html_writer::end_div();

        echo $output;
    }

}