<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace report_coursequizzes\output;

defined('MOODLE_INTERNAL') || die();

/**
 * Index renderer class
 *
 * @package   report_coursequizzes\output
 * @author    John Hoopes <john.hoopes@wisc.edu>
 * @copyright 2015 University of Wisconsin - Madison
 */
class index_renderer extends \plugin_renderer_base{


    /**
     * show the header of the page, with tabs and message
     *
     * @param $course
     * @throws \coding_exception
     */
    public function index_header($course){

        $this->page->requires->jquery();
        // require HTML5 classList shim to add classlist functionality to older browsers
        $this->page->requires->js('/report/coursequizzes/js/classList.js');
        $this->page->requires->js('/report/coursequizzes/js/index.js');

        echo $this->output->header();
        //echo report_coursequizzes_view_tabs($course->id, 'coursequizzes');
        $this->showMessage();
    }

    /**
     * Output the footer of the page
     */
    public function index_footer(){
        echo $this->output->footer();
    }

    /**
     * Sets a page message to display when the page is loaded into view
     *
     * base_header() must be called for the message to appear
     *
     * @param string $type
     * @param string $message
     */
    public function setMessage($type, $message) {
        $this->pageMessage = array($type, $message);
    }

    /**
     * Shows the message to the page.  is called by index_header
     */
    protected function showMessage(){

        if(empty($this->pageMessage)){
            return;
        }

        if (!is_array($this->pageMessage)) {
            return; // return if it's not an array
        }
        switch ($this->pageMessage[0]) {
            case 'error':
                echo $this->output->notification($this->pageMessage[1], 'notifiyproblem');
                break;
            case 'success':
                echo $this->output->notification($this->pageMessage[1], 'notifysuccess');
                break;
            case 'info':
                echo $this->output->notification($this->pageMessage[1], 'notifyinfo');
                break;
            default:
                // unrecognized notification type
                break;
        }
    }

    /**
     * render the index home page
     *
     * @param \stdClass $course
     * @param int $categoryid
     * @param int $contextid
     * @param \moodle_url $pageurl
     * @param \question_edit_contexts $contexts
     * @param array $categorytree
     */
    public function render_index_home($course, $categoryid, $contextid, $pageurl, $contexts, $categorytree) {

        echo \html_writer::start_div('questionrepository');

        $this->category_list_for_contexts($contexts, $pageurl, $categoryid . ',' . $contextid);

        $categorytreehtml = $this->render_category_tree($categorytree, $course, $contexts, $pageurl);

        $printquizzesurl = new \moodle_url('/report/coursequizzes/printquestions.php');
        $printquizsubmitbtn = \html_writer::empty_tag('input', array(
                                                                    'type' => 'submit',
                                                                    'name' => 'action',
                                                                    'value' => 'Print Questions',
                                                                    'class' => 'form-submit',
                                                                    'formaction' => $printquizzesurl->out_omit_querystring()
                                                                    ));
        $addtoquizurl = new \moodle_url('/report/coursequizzes/addtoquiz.php');
        $addtoquizsubmitbtn = \html_writer::empty_tag('input', array(
                                                                    'type' => 'submit',
                                                                    'name' => 'action',
                                                                    'value' => 'Add to Quiz',
                                                                    'class' => 'form-submit',
                                                                    'formaction' => $addtoquizurl->out_omit_querystring()
                                                                ));

        $buttonsdiv = \html_writer::div($printquizsubmitbtn . '&nbsp;&nbsp;' . $addtoquizsubmitbtn, 'formsubmit');

        // add url params as a hidden item to the form
        $hiddenparams = '';
        foreach($pageurl->params() as $name => $param){
            $hiddenparams .= \html_writer::empty_tag('input', array('type' => 'hidden', 'name' => $name, 'value' => $param));
        }

        echo \html_writer::start_tag('form', array('method'=>'post', 'action'=>$pageurl->out_omit_querystring()));
        echo $hiddenparams;
        echo $buttonsdiv;

        echo \html_writer::empty_tag('input', array('type' => 'hidden', 'name'=>'questionids'));

        echo \html_writer::end_tag('form');

        echo $categorytreehtml;



        echo \html_writer::end_div();
    }

    /**
     * renders a category list for the contexts.
     * This dropdown will be the "root" category that the tree will use as a root
     * to show lower categories.
     *
     * @param \question_edit_contexts $contexts
     * @param \moodle_url $pageurl
     * @param string $categoryandcontext The categoryid and context id together separated by a comma
     */
    protected function category_list_for_contexts($contexts, $pageurl, $categoryandcontext) {


        echo \html_writer::start_div('choosecategory');

        $catmenu = question_category_options($contexts->having_one_edit_tab_cap('editq'), true, 0, true);
        $cateselect = new \single_select($pageurl, 'selectacategory', $catmenu, $categoryandcontext);
        $cateselect->set_label(get_string('selectacategory', 'question'));
        echo $this->output->render($cateselect);

        echo \html_writer::end_div() . "\n";

    }

    protected function render_category_tree($categories, $course, $contexts, $pageurl){

        $content = '';

        foreach($categories as $category){
            $childrencontent = '';

            // recurse through category children until there are no more children of the categories
            if(!empty($category->children)){
                $childrencontent = $this->render_category_tree($category->children, $course, $contexts, $pageurl);
            }

            $questionbank = $this->render_category_questionbank($course, $contexts, $pageurl, $category->id . ',' . $category->contextid);
            $content .= $this->category_section($category->id, $category->name, $questionbank . $childrencontent);

        }

        return $content;
    }

    /**
     * Builds a category section
     *
     * @param string $id The id attribute to attach to the panel
     * @return string HTML string of the category section
     */
    protected function category_section($id, $name, $content){

        $catcontent = '';

        $catcontent .= \html_writer::start_div('panel panel-category', array('id'=>'panel_' . $id));

        $catcontent .= \html_writer::start_div('panel-heading');
        $pixicon = new \pix_icon('t/collapsed', 'open', 'moodle', array('class' => 'expandicon',
                                                                        'id' => 'panel_' . $id . '_expandicon'));
        $catcontent .= \html_writer::span($this->render($pixicon) . $name, 'panel-title');
        $catcontent .= \html_writer::end_div();

        $catcontent .= \html_writer::start_div('panel-body hidden');
        $catcontent .= $content;
        $catcontent .= \html_writer::end_div();

        $catcontent .= \html_writer::end_div();

        return $catcontent;
    }

    protected function render_category_questionbank($course, $contexts, $pageurl, $categoryandcontext){


        $qperpage = optional_param('qperpage', 100, PARAM_INT);
        $qpage = optional_param('qpage', 0, PARAM_INT);


        ob_start(); // capture question bank display in buffer to have the renderer render output

        $questionbank = new \report_coursequizzes\local\question_bank_view($contexts, $pageurl, $course, null);
        $questionbank->display('editq', $qpage, $qperpage, $categoryandcontext, false, false, true);

        return ob_get_clean();

    }



}