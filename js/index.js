

var coursequizzes = coursequizzes || {};

coursequizzes.init = function() {
    'use strict';

    var panels = document.getElementsByClassName('panel');

    for(var x = 0; x < panels.length; x++){
        panels[x].children[0].onclick = function(){
            var panelid = this.parentElement.getAttribute('id');
            coursequizzes.toggle_panel(panelid);
        }
    }



};

coursequizzes.toggle_panel = function(id){
    'use strict';

    // expand/contract the panel body as well as rotate the collapsed icon
    var panel = document.getElementById(id);
    var expandicon = document.getElementById(id + '_expandicon');

    if(panel.children[1].classList.contains('hidden')){
        panel.children[1].classList.remove('hidden');
        expandicon.classList.add('open');
    }else{
        panel.children[1].classList.add('hidden');
        expandicon.classList.remove('open');
    }
};

(function preload(){
    window.addEventListener('load', function(){coursequizzes.init();}, false);
}());


coursequizzes.updateSelection = function(questionid){

    var elements = document.getElementsByName('questionids');
    var input = elements[0];

    var currquestionids = input.getAttribute('value');

    if(currquestionids === null || currquestionids === ''){
        currquestionids = [];
    }else{
        currquestionids = currquestionids.split(',');
    }


    var qidIndex = currquestionids.indexOf(questionid);
    if(qidIndex === -1){
        currquestionids.push(questionid);
    }else{
        currquestionids.splice(qidIndex, 1);
    }

    input.value = currquestionids.join(',');


};


function in_array(needle, haystack, argStrict) {
    //  discuss at: http://phpjs.org/functions/in_array/
    // original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // improved by: vlado houba
    // improved by: Jonas Sciangula Street (Joni2Back)
    //    input by: Billy
    // bugfixed by: Brett Zamir (http://brett-zamir.me)
    //   example 1: in_array('van', ['Kevin', 'van', 'Zonneveld']);
    //   returns 1: true
    //   example 2: in_array('vlado', {0: 'Kevin', vlado: 'van', 1: 'Zonneveld'});
    //   returns 2: false
    //   example 3: in_array(1, ['1', '2', '3']);
    //   example 3: in_array(1, ['1', '2', '3'], false);
    //   returns 3: true
    //   returns 3: true
    //   example 4: in_array(1, ['1', '2', '3'], true);
    //   returns 4: false

    var key = '',
        strict = !! argStrict;

    //we prevent the double check (strict && arr[key] === ndl) || (!strict && arr[key] == ndl)
    //in just one for, in order to improve the performance
    //deciding wich type of comparation will do before walk array
    if (strict) {
        for (key in haystack) {
            if (haystack[key] === needle) {
                return true;
            }
        }
    } else {
        for (key in haystack) {
            if (haystack[key] == needle) {
                return true;
            }
        }
    }

    return false;
}