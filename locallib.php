<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Local lib
 *
 * @package   report_coursequizzes
 * @author    John Hoopes <hoopes@wisc.edu>
 * @copyright 2014 University of Wisconsin - Madison
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


/**
 * Prints local lib tabs
 *
 * @param \mod_activequiz\activequiz $RTQ Realtime quiz class
 * @param                            $currenttab
 *
 * @return string HTML string of the tabs
 */
function report_coursequizzes_view_tabs($courseid, $currenttab) {
    $tabs = array();
    $row = array();
    $inactive = array();
    $activated = array();

    $row[] = new tabobject('coursequizzes', new moodle_url('/mod/activequiz/view.php', array('course' => $courseid)), get_string('question repository', 'report_coursequizzes'));
    $row[] = new tabobject('addhocquestions', new moodle_url('/mod/activequiz/edit.php', array('course' => $courseid)), get_string('', 'report_coursequizzes'));


    if ($currenttab == 'responses') {
        $activated[] = 'responses';
    }

    if ($currenttab == 'edit') {
        $activated[] = 'edit';
    }

    if ($currenttab == 'view') {
        $activated[] = 'view';
    }

    return print_tabs($tabs, $currenttab, $inactive, $activated, true);
}
