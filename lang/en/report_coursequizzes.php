<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Language Strings
 *
 * @package    report
 * @subpackage wiscphotos
 * @copyright  2013 John Hoopes
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

$string['pluginname'] = 'Course Quizzes';
$string['pluginname_desc'] = 'Provides an interface to view a course\'s quiz and view them for printing.  As well as allows a user to print questions ad-hoc';

$string['coursequizzes:view'] = 'View the course quizzes report';
$string['questionaddheading'] = 'Questions to be added:';
$string['quizaddheading'] = 'Add to which Quiz:';
$string['questionsaddedsuccess'] = 'Questions added succesfully';




